<?php

/**
 * @file
 * Test case for universal_reference module
 */

/**
 * Tests the relevant functionality provided by the universal_reference module.
 */
class UniversalReferenceWebTestCase extends DrupalWebTestCase {

  /**
   * Info for simpletest.module.
   */
  public static function getInfo() {
    return array(
      'name' => 'Universal Reference',
      'description' => 'Ensure that the universal reference field works as expected',
      'group' => 'Universal Reference',
    );
  }

  /**
   * Setup for individual tests.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp(array('universal_reference'));

    // Creating the needed user.
    $this->privileged_user = $this->drupalCreateUser(
      array(
        'bypass node access',
        'administer nodes',
        'administer taxonomy',
        'edit terms in 1',
      ));
    $this->drupalLogin($this->privileged_user);

    // Creating the needed field on the article node type.
    $field = array(
      'field_name' => 'field_content_reference',
      'type' => 'universal_reference',
      'cardinality' => 1,
      'settings' => array(
        'referenceable_types' => array(
          array(
            'article' => 'article',
          ),
        ),
      ),
    );
    field_create_field($field);

    $instance = array(
      'field_name' => 'field_content_reference',
      'entity_type' => 'node',
      'label' => 'Content reference',
      'required' => 0,
      'bundle' => 'article',
      'description' => '',
      'widget' => array(
        'type' => 'title_and_reference',
        'weight' => -4,
        'settings' => array(
          'autocomplete_match' => 'contains',
        ),
      ),
      'display' => array(
        'default' => array(
          'type' => 'link_with_title',
          'weight' => 10,
        ),
        'teaser' => array(
          'type' => 'link_with_title',
          'weight' => 10,
        ),
      ),
    );
    field_create_instance($instance);

    // Creating a node used for the reference tests.
    $node = $this->drupalCreateNode(
        array(
          'type' => 'article',
          'title' => 'Node to be found',
          'body' => array(LANGUAGE_NONE => array(array('value' => 'THIS IS THE BODY TEXT'))),
          'field_summary' => array(LANGUAGE_NONE => array(array('value' => 'THIS IS THE SUMMARY TEXT'))),
          'field_teaser' => array(LANGUAGE_NONE => array(array('value' => 'THIS IS THE TEASER TEXT'))),
          'field_teaser_link' => array(LANGUAGE_NONE => array(array('url' => 'http://example.com'))),
        )
    );
    $nid = $node->nid;
    $this->original_node = $node;
    $this->drupalGet('node/' . $nid);
  }

  /**
   * Tests that the universal_reference field works as expected.
   */
  public function testAll() {
    $langcode = LANGUAGE_NONE;

    // Testing autocomplete field by looking for the created node and saving the
    // returned reference.
    $results = $this->drupalGetAJAX('universal_reference/autocomplete/node/article/field_content_reference/be found');
    $reference_value = current(array_keys($results));
    $ref_match = preg_match('/Node to be found \[nid:[0-9]+\]/', $reference_value);
    $this->assertTrue($ref_match, 'The original node should be found by the autocomplete service');

    // Creating node that referes to the original, with original title.
    $edit = array();
    $edit['title'] = "Node that refers to another node";
    $edit["field_content_reference[$langcode][0][nid]"] = $reference_value;
    $edit["body[$langcode][0][value]"] = 'BODY TEXT';

    $this->drupalPost('node/add/article', $edit, t('Save'));

    $node = $this->drupalGetNodeByTitle('Node that refers to another node');

    $refset = isset($node->field_content_reference[LANGUAGE_NONE][0]['nid']);
    $this->assertTrue($refset, 'A node reference should exist in the new node');

    if ($refset) {
      $this->assertTrue($this->original_node->nid == $node->field_content_reference[LANGUAGE_NONE][0]['nid'], 'New node should refer to the original one');
      $this->drupalGet('node/' . $node->nid);
      $this->assertText('Node to be found', 'Original node title should be shown as link');
    }

    // Creating node that referes to the original, with custom title.
    $edit = array();
    $edit['title'] = "New node that refers to another node";
    $edit["field_content_reference[$langcode][0][nid]"] = $reference_value;
    $edit["field_content_reference[$langcode][0][title]"] = 'Alternative title of referred node';
    $edit["body[$langcode][0][value]"] = 'BODY TEXT';

    $this->drupalPost('node/add/article', $edit, t('Save'));

    $node = $this->drupalGetNodeByTitle('New node that refers to another node');

    $refset = isset($node->field_content_reference[LANGUAGE_NONE][0]['nid']);
    $this->assertTrue($refset, 'A node reference should exist in the new node');

    if ($refset) {
      $this->assertTrue($this->original_node->nid == $node->field_content_reference[LANGUAGE_NONE][0]['nid'], 'New node should refer to the original one');
      $this->drupalGet('node/' . $node->nid);
      $this->assertText('Alternative title of referred node', 'Modified title should be shown as link');
    }

    // Creating node that referes to an external URL, with URL as link title.
    $edit = array();
    $edit['title'] = "Node that refers to an URL";
    $edit["field_content_reference[$langcode][0][nid]"] = 'http://www.reload.dk';
    $edit["body[$langcode][0][value]"] = 'BODY TEXT';

    $this->drupalPost('node/add/article', $edit, t('Save'));

    $node = $this->drupalGetNodeByTitle('Node that refers to an URL');

    $refset = isset($node->field_content_reference[LANGUAGE_NONE][0]['url']);
    $this->assertTrue($refset, 'An URL reference exist in the new node');

    if ($refset) {
      $this->assertTrue('http://www.reload.dk' == $node->field_content_reference[LANGUAGE_NONE][0]['url'], 'New node should refer to www.reload.dk');
      $this->drupalGet('node/' . $node->nid);
      $this->assertText('http://www.reload.dk', 'URL should be shown as link');
    }

    // Creating node that referes to an external URL, with custom link title.
    $edit = array();
    $edit['title'] = "New node that refers to an URL";
    $edit["field_content_reference[$langcode][0][nid]"] = 'http://www.reload.dk';
    $edit["field_content_reference[$langcode][0][title]"] = 'Alternative title of external link';
    $edit["body[$langcode][0][value]"] = 'BODY TEXT';

    $this->drupalPost('node/add/article', $edit, t('Save'));

    $node = $this->drupalGetNodeByTitle('New node that refers to an URL');

    $refset = isset($node->field_content_reference[LANGUAGE_NONE][0]['url']);
    $this->assertTrue($refset, 'An URL reference exist in the new node');

    if ($refset) {
      $this->assertTrue('http://www.reload.dk' == $node->field_content_reference[LANGUAGE_NONE][0]['url'], 'New node should refer to www.reload.dk');
      $this->drupalGet('node/' . $node->nid);
      $this->assertText('Alternative title of external link', 'Custom title should be shown as link');
    }
  }

}
